module gitlab.com/syifan/multi_gpu_experiments

require (
	gitlab.com/akita/akita v1.2.1
	gitlab.com/akita/gcn3 v1.3.1
	gitlab.com/akita/mem v1.1.2
	gitlab.com/akita/noc v1.1.2
)

//replace gitlab.com/akita/gcn3 => /Users/yifan/dev/src/gitlab.com/akita/gcn3
