package workloads

import (
	"flag"
	"log"

	"gitlab.com/akita/gcn3/benchmarks"
	"gitlab.com/akita/gcn3/benchmarks/amdappsdk/bitonicsort"
	"gitlab.com/akita/gcn3/benchmarks/amdappsdk/matrixmultiplication"
	"gitlab.com/akita/gcn3/benchmarks/amdappsdk/matrixtranspose"
	"gitlab.com/akita/gcn3/benchmarks/amdappsdk/simpleconvolution"
	"gitlab.com/akita/gcn3/benchmarks/dnn/maxpooling"
	"gitlab.com/akita/gcn3/benchmarks/dnn/relu"
	"gitlab.com/akita/gcn3/benchmarks/heteromark/aes"
	"gitlab.com/akita/gcn3/benchmarks/heteromark/fir"
	"gitlab.com/akita/gcn3/benchmarks/heteromark/kmeans"
	"gitlab.com/akita/gcn3/driver"
)

var benchmarkName = flag.String("benchmark", "fir", "The benchmark to run")

func GetWorkload(d *driver.Driver) benchmarks.Benchmark {
	switch *benchmarkName {
	case "aes":
		b := aes.NewBenchmark(d)
		b.Length = 1048576
		return b
	case "bs", "bitonicsort":
		b := bitonicsort.NewBenchmark(d)
		b.Length = 4096
	case "fir":
		b := fir.NewBenchmark(d)
		b.Length = 1048576
		return b
	case "km", "kmeans":
		b := kmeans.NewBenchmark(d)
		b.NumPoints = 2048
		b.NumFeatures = 64
		b.NumClusters = 5
		b.MaxIter = 20
		return b
	case "mm", "matrixmultiplication":
		b := matrixmultiplication.NewBenchmark(d)
		b.X = 2048
		b.Y = 2048
		b.Z = 2048
		return b
	case "mt", "matrixtranspose":
		b := matrixtranspose.NewBenchmark(d)
		b.Width = 4096
		return b
	case "mp", "maxpooling":
		b := maxpooling.NewBenchmark(d, 1, 1, 512, 512)
		return b
	case "rl", "relu":
		b := relu.NewBenchmark(d)
		b.Length = 1048576
		return b
	case "sc", "simpleconvolution":
		b := simpleconvolution.NewBenchmark(d)
		b.Width = 2048
		b.Height = 2048
		b.SetMaskSize(3)
		return b
	default:
		log.Panicf("unknown benchmark name %s\n", *benchmarkName)
	}
	panic("never")
}
