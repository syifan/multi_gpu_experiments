package main

import (
	"flag"
	"fmt"
	"sync"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/benchmarks"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/platform"
)

var numGPUs = flag.Int("num-gpus", 1, "The number of GPUs to use")

type Runner struct {
	Engine            akita.Engine
	GPUDriver         *driver.Driver
	KernelTimeCounter *driver.KernelTimeCounter
	Benchmarks        []benchmarks.Benchmark
}

func (r *Runner) Init() {
	r.buildPlatform()
	r.KernelTimeCounter = driver.NewKernelTimeCounter()
	r.GPUDriver.AcceptHook(r.KernelTimeCounter)
	r.GPUDriver.Run()
}

func (r *Runner) AddBenchmark(b benchmarks.Benchmark) {
	gpus := make([]int, *numGPUs)
	for i := 0; i <*numGPUs; i++ {
		gpus[i] = i + 1
	}
	b.SelectGPU(gpus)
	r.Benchmarks = append(r.Benchmarks, b)
}

func (r *Runner) Run() {
	var wg sync.WaitGroup
	for _, b := range r.Benchmarks {
		wg.Add(1)
		go func(b benchmarks.Benchmark, wg *sync.WaitGroup) {
			b.Run()
			b.Verify()
			wg.Done()
		}(b, &wg)
	}
	wg.Wait()

	r.Engine.Finished()
	fmt.Printf("Kernel time: %.12f\n", r.KernelTimeCounter.TotalTime)
	fmt.Printf("Total time: %.12f\n", r.Engine.CurrentTime())
}

func (r *Runner) buildPlatform() {
	r.Engine, r.GPUDriver = platform.BuildNR9NanoPlatform(4)
}
